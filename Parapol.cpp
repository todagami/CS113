#include "Parapol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	int x, y, p;
	x = 0; y = 0;
	p = 1 - A;
	while (y < A)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (p >= 0)
		{
			x = x + 1;
			p = p - 2 * A;
		}
		y = y + 1;
		p = p + 2 * y + 1;
	}
	if (p == 1)
		p = 1 - 4 * A;
	else
		p = 1 - 2 * A;
	while (y > A)
	{
		Draw2Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			y = y + 1;
			p = p + 4 * y;
		}
		x = x + 1;
		p = p - 4 * A;
	}
}
//wtf

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
}
